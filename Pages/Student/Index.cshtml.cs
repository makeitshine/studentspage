using System;
using System.Collections.Generic;
using System.Linq;
using RazorPages.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using RazorPages.Data;



namespace RazorPages.Pages.Student
{ 
    public class IndexModel : PageModel
    {
        private readonly RazorPagesDbContext _context;

        public IndexModel(RazorPagesDbContext context)
        {
            _context = context;
        }

        public IList<Models.Student> Students { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        
        public SelectList FirstNames { get; set; }
        [BindProperty(SupportsGet = true)]
        public string StudentsFirstName { get; set; }

        public async Task OnGetAsync()
        {
            var students = from m in _context.Students
                         select m;
            if (!string.IsNullOrEmpty(SearchString))
            {
                SearchString.ToLower();
                students = students.Where(s => s.LastName.Contains(SearchString) | s.LastName.ToLower().Contains(SearchString) );
            }

            Students = await students.ToListAsync();
        }

        public async Task OnPostGreaterThanAsync(int gpa)
        {
            var students = from m in _context.Students
                           select m;
            
            students = students.Where(s => s.GPA>gpa);
        
            Students = await students.ToListAsync();
        }
        public async Task OnPostLessThanAsync(int gpa)
        {
            var students = from m in _context.Students
                           select m;

            students = students.Where(s => s.GPA < gpa);

            Students = await students.ToListAsync();
        }


    }
}