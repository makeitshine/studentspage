using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPages.Pages.Student
{
    public class CreateModel : PageModel
    {
        private readonly RazorPages.Data.RazorPagesDbContext _db;

        public CreateModel(RazorPages.Data.RazorPagesDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Models.Student Student { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _db.Students.Add(Student);
            await _db.SaveChangesAsync();
            return RedirectToPage("/Index");
        } 
    
    }
}